#
# Compiler command (use full path if not in PATH)
CC=g++

#
# SDL Locations and arguments
SDL2_DIR = ../sdl2
SDL2_INC = -I$(SDL2_DIR)/include
SDL2_LIB = -L$(SDL2_DIR)/lib -lmingw32 -lSDL2main -lSDL2 -mwindows

#
# Directories
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./obj

#
# The source files of the project
_OBJ_FILES = main.o
OBJ_FILES = $(patsubst %,$(OBJ_DIR)/%,$(_OBJ_FILES))

#
# Compile flags
CFLAGS=-I$(INC_DIR)

#
# The name of the program/game
PROGRAM_NAME=gameName

#
# Start of build definitions
# Don't change this one
all: $(OBJ_FILES)
	$(CC) -o $(PROGRAM_NAME) $< $(SDL2_LIB) $(CFLAGS)

# Add all of the individual .o files here
# When you add one, be sure to add it to _OBJ_FILES above
# Also note that they all must be a different name
$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp
	$(CC) -c -o $@ $< $(SDL2_INC) $(CFLAGS)

# Don't change this one either
clean:
	rm -f $(OBJ_DIR)/*.o

